using Toybox.WatchUi as Ui;
using Toybox.Graphics as Gfx;
using Toybox.Application as App;
using Toybox.FitContributor as Fit;

class VirtualPartnerDataFieldView extends Ui.DataField {

	hidden var distanceParameter;
	hidden var allureParameter;
	hidden var estimateTime=0;
	hidden var estimateTimeRest=0;
	hidden var gapTime=0;
	hidden var gapTimeLap=0;
	hidden var runnerIcon;
	hidden var virtualPartnerIcon;	
	hidden var heightFontMedium;
	hidden var heightFontTiny;
	hidden var display;
	hidden var avaTps;
	hidden var retTps;
	hidden var chronoEstimate;
	hidden var tpsRest;
	hidden var pace;
	hidden var paceRef;
	hidden var errorFormat=false;
	hidden var timeScalePartner;
	hidden var allureRunner;
	hidden var unitKm;
	hidden var onlyDataField;
	hidden var distanceRunner=0;
	hidden var distanceLap=0;
	hidden var timeLap=0;
	hidden var timeRunner = null;
	hidden var allureField = null;
	const ALLURE_FIELD_ID = 0;
	
    function initialize() {
        var metric = System.getDeviceSettings().distanceUnits;
		unitKm = metric==System.UNIT_METRIC;
		
        runnerIcon = Ui.loadResource(Rez.Drawables.Runner);
        virtualPartnerIcon = Ui.loadResource(Rez.Drawables.VirtualPartner);
        avaTps = Ui.loadResource(Rez.Strings.AvaTps);
        retTps = Ui.loadResource(Rez.Strings.RetTps);
        chronoEstimate = Ui.loadResource(Rez.Strings.ChronoEstimate);
        tpsRest = Ui.loadResource(Rez.Strings.TpsRest);
        if(unitKm){
        	pace = Ui.loadResource(Rez.Strings.PaceKm);
        	paceRef = Ui.loadResource(Rez.Strings.PaceRefKm);
        }else{
        	pace = Ui.loadResource(Rez.Strings.PaceM);
        	paceRef = Ui.loadResource(Rez.Strings.PaceRefM);
        }
        
        distanceParameter = App.getApp().getProperty("distance").toFloat()*1000;
        if(!unitKm){
			distanceParameter=distanceParameter*1.60934;
		}
        display = App.getApp().getProperty("display");
        var scalePartner = App.getApp().getProperty("scalePartner");

        var timeParameter = App.getApp().getProperty("time");
	
		var timeExtract = extract(timeParameter);
		if(distanceParameter <0 ||timeExtract[0]==null || timeExtract[1]==null || timeExtract[2]==null){
			errorFormat=true;
			allureParameter = 0;
			distanceParameter = 0;
		}else{
			errorFormat=false;
			var paceParameter = App.getApp().getProperty("pace");
			var allureExtract = extractPace(paceParameter);
			
			if(allureExtract[0]==null || allureExtract[0]*60+allureExtract[1]==0){
				var time = 	(timeExtract[0]*60*60+timeExtract[1]*60+timeExtract[2])*1000;
				allureParameter = time/distanceParameter;
			}else{
				allureParameter = allureExtract[0]*60+allureExtract[1];
				if(!unitKm){
					allureParameter = 0.621371*allureParameter;
				}
			}
		}
		
		var scalePartnerExtract = extract(scalePartner);
		if(scalePartnerExtract[0]==null || scalePartnerExtract[1]==null || scalePartnerExtract[2]==null){
			timeScalePartner = 3600;
		}else{
			timeScalePartner = 	scalePartnerExtract[0]*60*60+scalePartnerExtract[1]*60;
		
		}
		onlyDataField = App.getApp().getProperty("onlyDataField");
		
		DataField.initialize();
		allureField = createField(
            Ui.loadResource(Rez.Strings.paceTitle),
            ALLURE_FIELD_ID,
            FitContributor.DATA_TYPE_STRING,
            {:mesgType=>Fit.MESG_TYPE_SESSION, :units=>"mm:ss",:count=>8}
        );

        allureField.setData(timeToString(allureParameter.toLong()));
    }
	
	function extract(timeString){
		var timeExtract = new [3];
		var separatorIndex;
		for( var i=0;i<3;i++){
			separatorIndex = timeString.find(":");
			if(separatorIndex!=null){
				timeExtract[i] = timeString.substring(0, separatorIndex).toNumber();
				timeString = timeString.substring(separatorIndex+1,timeString.length());
			}else if(i==2 && timeExtract[0]!=null && timeExtract[1]!=null){
				timeExtract[i] = timeString.toNumber();
			}
		}
		return timeExtract;
	}

	function extractPace(timeString){
		var timeExtract = new [2];
		var separatorIndex;
		for( var i=0;i<2;i++){
			separatorIndex = timeString.find(":");
			if(separatorIndex!=null){
				timeExtract[i] = timeString.substring(0, separatorIndex).toNumber();
				timeString = timeString.substring(separatorIndex+1,timeString.length());
			}else if(i==1 && timeExtract[0]!=null){
				timeExtract[i] = timeString.toNumber();
			}
		}
		return timeExtract;
	}
	
    function onLayout(dc) {
		heightFontMedium = dc.getFontHeight(Gfx.FONT_NUMBER_MEDIUM);
		heightFontTiny = dc.getFontHeight(Gfx.FONT_SYSTEM_TINY);
    }

    function compute(info) {
    	if(info.elapsedDistance!=null){
    		distanceRunner = info.elapsedDistance;
    	}
		
		var distanceRunnerLap = distanceRunner-distanceLap;
		
		if(App.getApp().getProperty("timer") == 0){
			timeRunner = info.elapsedTime;
		}else{
			timeRunner = info.timerTime;
		}
		
		var timeRunnerLap = timeRunner-timeLap;
		if(timeRunner!=null && distanceRunner!=0){
	 		allureRunner = timeRunner/distanceRunner;
	 	}else{
	 		distanceRunner=0;
			allureRunner=0;  
	 	}
	
		var allureRunnerLap=0;		 

		if(timeRunnerLap!=null && distanceRunnerLap!=0){
	 		allureRunnerLap = timeRunnerLap/distanceRunnerLap;
	 	}else{
	 		distanceRunnerLap=0;
			allureRunnerLap=0;  
	 	}
	 	
		var distanceToDestination = distanceParameter - distanceRunner;
		estimateTimeRest = (allureRunner*distanceToDestination/1000).toLong();
		estimateTime = (allureRunner*distanceParameter/1000).toLong();
		gapTime = (Math.round((allureRunner*distanceRunner-allureParameter*distanceRunner)/1000)).toLong();
		gapTimeLap = (Math.round((allureRunnerLap*distanceRunnerLap-allureParameter*distanceRunnerLap)/1000)).toLong();
		if(!unitKm){
			allureRunner = 1.60934*allureRunner;
		}
	
		if(App.getApp().getProperty("resetOnLap")){
			gapTime = gapTimeLap;
			allureRunner = allureRunnerLap;
		}
    }

    function onUpdate(dc) {
    	dc.clear();
        var center_x = dc.getWidth()/2;
    	var center_y = dc.getHeight()/2;
    	var y = dc.getHeight()/3;
    	
    	dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_WHITE);
		dc.fillRectangle(0,0,dc.getWidth(),dc.getHeight());
		dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_WHITE);
    	if(errorFormat){
    		dc.drawText(center_x, center_y, Gfx.FONT_SYSTEM_SMALL, "Error parsing format",  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);				
    	}else{
			if(onlyDataField){
				
	    		dc.drawLine(0, y, dc.getWidth(), y);
				dc.drawLine(0, y*2, dc.getWidth(), y*2);
	    		var gap="";
	    		var gapRunner = center_y*gapTime/timeScalePartner;
				var displayTime;
	    		if(distanceRunner==0){
					dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
					gap=paceRef;
					displayTime=allureParameter.toLong();
				}else if(gapTime>0){
					dc.fillRectangle(0, 0, dc.getWidth(), y);
					dc.fillRectangle(0, y*2, dc.getWidth(), dc.getHeight());
					dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_TRANSPARENT);
					gap=retTps;
					displayTime=gapTime;
				}else{
					gapTime=gapTime*-1;
					dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
					gap=avaTps;
					displayTime=gapTime;
				}
				var font=Gfx.FONT_NUMBER_MEDIUM;
				
				if(heightFontMedium<=45 && heightFontMedium>=y*0.70){
					font=Gfx.FONT_NUMBER_MILD;
				}
				
				dc.drawText(center_x, y/2-y/4, Gfx.FONT_SYSTEM_TINY, gap,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
				dc.drawText(center_x, y/2+y/6,font, timeToString(displayTime), Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
			
				
					if(display == 0 && estimateTimeRest>=0){
						dc.drawText(center_x, y*2+y/2-y/2.8, Gfx.FONT_SYSTEM_TINY, tpsRest,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);		
						dc.drawText(center_x, y*2+y/2, font, timeToString(estimateTimeRest), Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
					}else if(display == 1){
						dc.drawText(center_x, y*2+y/2-y/2.8, Gfx.FONT_SYSTEM_TINY, chronoEstimate,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);		
						dc.drawText(center_x, y*2+y/2, font, timeToString(estimateTime), Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
					}else{
						dc.drawText(center_x, y*2+y/2-y/2.8, Gfx.FONT_SYSTEM_TINY, pace,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);		
						dc.drawText(center_x, y*2+y/2, font, timeToString(allureRunner.toLong()), Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
					}
				
			
				if(gapTime.abs()<timeScalePartner*0.1){
					gapRunner = 0;
				}
				dc.drawBitmap(center_x-virtualPartnerIcon.getWidth()/2+10+gapRunner,center_y-virtualPartnerIcon.getHeight()/2,virtualPartnerIcon);
				if(gapRunner > center_y-30){
					gapRunner=center_y-30;
				}else if(gapRunner<-(center_y-30)){
					gapRunner=-(center_y-30);
				}
				dc.drawBitmap(center_x-runnerIcon.getWidth()/2+(gapRunner*-1),center_y-runnerIcon.getHeight()/2,runnerIcon);
	    	}else{
	    		var gap="";
		    	if(gapTime>0){
					dc.fillRectangle(0, 0, dc.getWidth(), dc.getHeight());
					dc.setColor(Gfx.COLOR_WHITE,Gfx.COLOR_TRANSPARENT);
					gap=retTps;
				}else{
					gapTime=gapTime*-1;
					dc.setColor(Gfx.COLOR_BLACK,Gfx.COLOR_TRANSPARENT);
					gap=avaTps;
				}
				var heightFontMediumOpti = heightFontMedium*0.75f;
				var heightFontTinyOpti = heightFontTiny*0.75f;
				dc.drawText(center_x, center_y-(heightFontMediumOpti+heightFontTiny)/2+heightFontTinyOpti/2, Gfx.FONT_SYSTEM_TINY, gap,  Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
				dc.drawText(center_x, center_y-(heightFontMediumOpti+heightFontTiny)/2+heightFontTinyOpti+heightFontMediumOpti/2+4 ,Gfx.FONT_NUMBER_MEDIUM, timeToString(gapTime), Graphics.TEXT_JUSTIFY_CENTER|Graphics.TEXT_JUSTIFY_VCENTER);
	    	}
    	}
    }
        
    function timeToString(long){
		var seconds = long % 60;
		var minutes = (long / 60) % 60;
		var hour = long/60/60;
		if(hour>0){
			return hour+":"+minutes.format("%02d")+":"+seconds.format("%02d");
		}else{
			return minutes+":"+seconds.format("%02d");
		}
	}
	
	function onWorkoutStepComplete(){
		distanceLap=distanceRunner;
		timeLap=timeRunner;
	}
	
	function onTimerLap(){
		distanceLap=distanceRunner;
		timeLap=timeRunner;
	}
}
